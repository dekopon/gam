//gam.cpp file
//handles the main loop and joins everything together

#include "gam.h"


int main() {

  Gam gam;

  if (gam.init()) {
    gam.launch();
  } //end if statement

  gam.cleanup();

} //end main function




Gam::Gam() {
  exit = false; //when set to true, game will attempt to exit main loop
} //end Gam constructor


bool Gam::init() {
  //Do anything that needs to be done before entering the main loop here.
  //Reason being constructors are potentially dangerous as they can't return
  //false if something wrong happens.
  //As of now, there doesn't seem to be a way for this function to return false...

  bool status = true; //if false, an error occurred

  //set up keybindings
  keybindings.autoInit();

  //set up event manger and listeners
  init_events();
  
  //set up window
  wm.createWindow(800,600);
  
  return status;

} //end init function



void Gam::launch() {
  //if init all goes well then do more things
  
  //Main loop
  while (!wm.exit_status()) {

    //get keystrokes and mouse movement+clicks
    while(wm.getWindow()->pollEvent(*im.getEvent()))
      im.getInput(); //send keystrokes and mouseclicks
    
    ed.resolve_queue(); //fire all events in queue
    ed.update_all(); //resolve the events fired by event distributor
    
    dm.draw(wm.getWindow()); //draw the results on screen
    
  } //end while loop
  
} //end launch function



void Gam::cleanup() {
  //because apparently some things like windows might need cleaning up

  if (wm.getWindow()->isOpen())
    wm.getWindow()->close();
  
} //end cleanup function


void Gam::init_events() {
  //initialize the event distributor and necessary event managers 
  EventDistributor* ped = &ed; //event distributor pointer declaration
  Keybindings* pkb = &keybindings; //keybindings pointer
  WindowManager* pwm = &wm; //windows manager pointer

  wm.init(ped);
  wm.subscribe();
  
  im.init(ped,pkb);
  im.subscribe();
  
  dm.init(ped,pwm);
  dm.subscribe();
  
  
} //end init_events function
