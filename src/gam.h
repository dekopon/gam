//gam.h file

#include <iostream>

#include "input.h"
#include "display.h"

#include "SFML/Graphics.hpp"

#ifndef EVENT_H
#define EVENT_H
#include "event.h"
#endif


class Gam {

  bool exit;
  Keybindings keybindings;

  //Event distributor and managers
  EventDistributor ed;
  WindowManager wm;
  InputManager im;
  DisplayManager dm;
  
  
 public:
  Gam();

  bool init();
  void init_events();
  void launch();
  void cleanup();

}; //end Gam class
