//event.h file

#include <cstring>
#include <queue>


class EventManager;
class EventDistributor;

class EventManager {
  //An EventManager interacts with the EventDistributor and its respective systems.
  //This class should be extended to utilize the handle_event function

 protected:
   EventDistributor* ed;

  std::vector<char const*> list_listening; //list of events to be processed if receieved
  std::queue<char const*> event_queue; //receieved events for processing

 public:
  EventManager();
  EventManager(EventDistributor*);

  //void init(EventDistributor*);
  
  bool subscribe();
  bool unsubscribe();
  
  bool add_listen(char const*);
  bool remove_listen(char const*);
  
  void fire_event(char const*);
  void receive_event(char const*);

  void resolve_queue();
  virtual void handle_event(char const*) = 0;

}; //end EventManager class


class EventDistributor {
  //The EventDistributor sends and receives events to and from any subscribing EventManager.

  std::vector<EventManager*> em_listening_list; //event managers listening
  
  std::vector<char const*> event_listening_list; //events being listened for
  std::vector<std::vector<EventManager*>> event_subscription_list; //corresponds with event_listen events a certain event manager is listening for
  std::queue<char const*> event_queue; //contains events queued for firing

  bool validateEvent(char const*);
  
 public:
  EventDistributor();

  //Functions involving sending or receiving
  bool add_listener(EventManager*);
  bool remove_listener(EventManager*);
  bool listen(char const*,EventManager*);
  bool stop_listen(char const*,EventManager*);
  void fire_event(char const*,EventManager*);
  bool receive_event(char const*);

  bool resolve_queue();
  void update_all();

  
}; //end EventDistributor class
