//event.cpp file


#include "event.h"



/***
 ***
Event Manager functions
 ***
 ***/

EventManager::EventManager(){}

EventManager::EventManager(EventDistributor* ined) {
  //this constructor may not be necessary
  ed = ined;
} //end EventManager constructor


bool EventManager::subscribe() {
  //Add this event manager to the event distributor's listening list.
  //Returns false if the event manager is already subscribed.
  return ed->add_listener(this);
} //end subscribe function


bool EventManager::unsubscribe() {
  //Remove self from event distributor's listening list.
  //Returns false if the event manager was not on the event distributor's list.
  return ed->remove_listener(this);
} //end unsubscribe function


bool EventManager::add_listen(char const * event) {
  //Add an event that the event manager wants to listen to.
  //Returns false if the event manager was already listening for the event.
  return ed->listen(event,this);
} //end add_listen function


bool EventManager::remove_listen(char const * event) {
  //Tell event distributor the event the event manager wants to stop listening to.
  //Returns false if the event manager was never listening for the event.
  return ed->stop_listen(event,this);
} //end remove_listen function


void EventManager::fire_event(char const* event) {
  //Send an event to the event distributor.
  ed->receive_event(event);
} //end fire_event function


void EventManager::receive_event(char const* event) {
  //Events the event manager was subscribed to are receieved here.

  event_queue.push(event);
  
} //end receive_event function


void EventManager::resolve_queue() {
  //Go through the event queue and process it accordingly.
  //It is intended that the handle_event function be extended to deal with events.
  //If checks are needed, this can become a bool function.
  
  while (!event_queue.empty()) {
    handle_event(event_queue.front());
    event_queue.pop();
  } //end while loop

} //end resolve_queue function






/***
 ***
Event Distributor functions
 ***
 ***/

EventDistributor::EventDistributor() {
} //end EventDistributor constructor


bool EventDistributor::validateEvent(char const* inevent) {
  //Validate the event and return true if event is valid.
  //A valid event has "Event_" in its name and does not exceed 64 characters.

  bool status = false;
  int endchar = 0;

  //for loop to check if the end character '\0' is within 64 characters
  for (unsigned int x = 0; ((x < 64)&&(!endchar)); x++) {
    if (inevent[x] == '\0') endchar = x;
  } //end for loop

  //check that the event begins with 'Event_'
  if (endchar > 7)
    if ((inevent[0] == 'E')&&
	(inevent[1] == 'v')&&
	(inevent[2] == 'e')&&
	(inevent[3] == 'n')&&
	(inevent[4] == 't')&&
	(inevent[5] == '_'))
      status = true;
  
  return status;

} //end validateEvent function


bool EventDistributor::add_listener(EventManager* inem) {
  //add the incoming listener(event manager) to the listening list
  //returns false if the listener is already on the list

  bool status = true;

  //go through entire list to check if event manager already exists
  for (unsigned int x = 0; x < em_listening_list.size(); x++)
    if (inem == em_listening_list[x])
      status = false;

  //if event manager was not already in the list, add it
  if (status)
    em_listening_list.push_back(inem);

  return status;
  
} //end add_listener function


bool EventDistributor::remove_listener(EventManager* inem) {
  //remove the incoming listener(event manager) from the listening list
  //returns false if the listener was never on the list

  bool status = false;

  //go through entire list to check if event manager exists
  for (unsigned int x = 0; x < em_listening_list.size(); x++) {
    if (inem == em_listening_list[x]) {
      em_listening_list.erase(em_listening_list.begin()+x);
      status = true;
    } //end if statement
  } //end for loop

  return status;
  
} //end remove_listener function


bool EventDistributor::listen(char const* inevent, EventManager* inem) {
  //add an event if it doesn't exist to the listening list and
  //the event manager to the subscription list OR if the event already exists,
  //just add the event manager that wants to listen to the subscription list
  //returns false if the event already exists and the event manager is already
  //listening to it
  
  bool event_existance = false;
  bool status = true;

  //check if event already exists in list
  for (unsigned int x = 0;(x < event_listening_list.size())&&(!event_existance);x++) {
    //check if event already exists
    if (!std::strcmp(inevent,event_listening_list[x])) {
      event_existance = true;
    } //end if statement

    //if event already exist, check if event manager is already listening for it
    if (event_existance) {

      //go through all event managers listening for this particular event
      for (unsigned int y = 0; y < event_subscription_list[x].size();y++)
	//if true, event manager was already listening for this event
	if (inem == event_subscription_list[x][y])
	  status = false;

      //event exists but event manager wasn't listening before
      if (status)
	event_subscription_list[x].push_back(inem);
      
    } //end if statement
  } //end for loop

  //create event if it was not in the list and then add the listener
  if (!event_existance) {
    event_listening_list.push_back(inevent);
    //event_subscription_list[event_listening_list.size()].push_back(inem);
    //Double the vector, double the push_back.
    //Kind of messy here. Maybe there's a better way to write all this.
    std::vector<EventManager*> temp;
    temp.push_back(inem);
    event_subscription_list.push_back(temp);
        
  } //end if statement
  
  return status;
  
} //end listen function


bool EventDistributor::stop_listen(char const* inevent, EventManager* inem) {
  //Remove the event manager from the event.
  //If no event manager is listening to an event anymore, remove the event.
  //Function returns false is event doesn't exist or the event manager wasn't listening for it in the first place.
  
  bool status = false;
  bool event_exists = false;
  
  //check for existance of event
  for (unsigned int x = 0; (x < event_listening_list.size())&&(!status); x++) {
    if (!std::strcmp(inevent,event_listening_list[x]))
      event_exists = true;

    //event is currently in list; check if event manager was listening to it
    if (event_exists) {
      for (unsigned int y = 0; y < event_subscription_list[x].size(); y++) {
	if (inem == event_subscription_list[x][y]) {
	  event_subscription_list[x].erase(event_subscription_list[x].begin()+y);
	  status = true;
	} //end if statement
      } //end for loop

      //check if event has 0 event mangers listening to it
      //if no event managers are listening to an event, remove event from the list
      if (status) {
	if (!event_subscription_list[x].size()) {
	  //delete vector elements via swap and pop
	  std::swap(event_listening_list[x],event_listening_list.back());
	  event_listening_list.pop_back();
	  std::swap(event_subscription_list[x],event_subscription_list.back());
	  event_subscription_list.pop_back();
	  
	} //end if statement
      } //end if statement
      
    } //end if statement
    
  } //end for loop

  return status;
  
} //end stop_listen function


void EventDistributor::fire_event(char const* inevent, EventManager* inem){
  //Fire an individual event to the passed event manager.
  //No check is made here if the event manager was listening for the event or not.
  
  inem->receive_event(inevent);
    
} //end fire_event function


bool EventDistributor::receive_event(char const* inevent) {
  //Validate received event and if valid, add the event to the event queue.
  bool status = false;

  if (validateEvent(inevent)) {
    event_queue.push(inevent);
    status = true;
  } //end if statement

  return status;

} //end receive_event function



bool EventDistributor::resolve_queue() {
  //Deal with the events in the event queue.
  //Function goes through the events that are queued up and if there is an
  //event manager that is listening for the event, the event distributor
  //will fire the event to it.
  //Function will return false if an event that no event manager was listening to was in the queue.
  
  bool status = false;
  bool eventfound = false;
  bool firedEvent = false;

  //while loop to go through all events queued
  while (!event_queue.empty()) {
    auto current = event_queue.front();

    //for loop to go through list of events being listened for
    for (unsigned int x = 0; x < event_listening_list.size(); x++) {
      if (!std::strcmp(current,event_listening_list[x])) {

	eventfound = true; //event in listening list was found
	firedEvent = false; //reset
	
	//for loop to go through list of event managers listening to this event
	for (unsigned int y = 0; y < event_subscription_list[x].size(); y++) {
	  //for loop to go through list of event managers that are subscribed
	  for (unsigned int z = 0; z < em_listening_list.size(); z++) {
	    //check if event manager was subscribed 
	    if (event_subscription_list[x][y] == em_listening_list[z]) {
	      //event manager was listening for event; fire the event to it
	      fire_event(current,event_subscription_list[x][y]);
	      firedEvent = true;
	    } //end if statement
	  } //end for loop
	} //end for loop

	//failed to fire event because of invalid or nonexistant event managers
	if (!firedEvent)
	  status = false;
	
      } //end if statement
    } //end for loop

    if (!eventfound)
      //an event was in the queue but no event manager was listening for it
      status = false;

    event_queue.pop();
  } //end while loop

  //the queue should be empty after resolving all events
  if (!event_queue.empty())
    status = false;
  
  return status;
  
} //end resolve_queue function


void EventDistributor::update_all() {
  //Function calls update function for all event managers listening for any events.
  for (unsigned int x = 0; x < em_listening_list.size(); x++)
    em_listening_list[x]->resolve_queue();

} //end update_all function
