//input.h

#include <iostream>
#include <fstream>
#include <cstring>

#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>


#ifndef EVENT_H
#define EVENT_H
#include "event.h"
#endif

class Key {

  std::string name; //key name (a,b,c,d... ctrl,shift,space,etc.)
  int sfk_index; //SFML keyboard index; corresponds to the Key enum for SFML

 public:
  Key();
  void initKey(std::string, int);

  std::string getName() const;
  int getIndex() const;
  
  void displayKeyInfo(bool) const;

}; //end Key class definition


class Keybindings {

  Key keylist[102]; //array storing all possible keys that can be used
  int bindings[9]; //array of keybinds storing int corresponding to the keylist array

  std::string autoInit_read();
  bool autoInit_parse(std::string);
  
 public:
  Keybindings();

  void autoInit();
  void defaultInit();
  bool validateBindings();
  void displayBindInfo();

  bool isKeyPressed(int);
  bool isKeybindPressed(int);

}; //end Keybindings class definition


class InputManager : public EventManager {

  Keybindings* kb;
  sf::Event event;
  
 public:
  void init(EventDistributor*,Keybindings*);
  sf::Event* getEvent();
  void handle_event(char const*);
  void getInput();

}; //end InputManager class declaration
