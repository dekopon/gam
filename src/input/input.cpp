//input.cpp
//handles the key bindings and input


#include "input.h"


Key::Key() {
  //set key to empty state

  name = "<Unset>";
  sfk_index = -1;

} //end Key oonstructor


void Key::initKey(std::string in_name, int in_index) {
  //set up key with passed information

  name = in_name;
  sfk_index = in_index;

} //end initKey function


std::string Key::getName() const { return name;}
int Key::getIndex() const {return sfk_index;}


void Key::displayKeyInfo(bool newline) const {
  //print out the key's name and sfk index
  std::cout << name << "\t" << sfk_index;
  if (newline)
    std::cout << std::endl;
} //end displayKeyInfo function





Keybindings::Keybindings() {
  //Set up the list of keys that can be bounded by assigning them
  //their names and index numbers. The order here is near identical to
  //SFML's key enum

  std::string nameList[102] = {
    //technically 'Unknown' is the first element with a value of -1
    "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",

    "Num0","Num1","Num2","Num3","Num4","Num5","Num6","Num7","Num8","Num9",

    "Escape","LControl","LShift","LAlt","LSystem","RControl","RShift","RAlt","RSystem","Menu","LBracket","RBracket","SemiColon","Comma","Period","Quote","Slash","BackSlash","Tilde","Equal","Dash","Space","Return","Backspace","Tab","PageUp","PageDown","End","Home","Insert","Delete","Add","Subtract","Multiply","Divide","Left","Right","Up","Down",

    "Numpad0","Numpad1","Numpad2","Numpad3","Numpad4","Numpad5","Numpad6","Numpad7","Numpad8","Numpad9",

    "F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15","Pause",

    "Unknown" //technically first with a value of -1

    //,"KeyCount" //this shouldn't even be a key
  };
  
  //initialize possible keys with the nameList array
  for (int x = 0; x < 101; x++) {
    keylist[x].initKey(nameList[x],x);
  } //end for loop
  keylist[101].initKey(nameList[101],-1); //'Unknown' aka <Unset>

  //set all bindings to Unknown
  for (int x = 0; x < 9; x++) {
    bindings[x] = 101;
  } //end for loop

} //end Keybindings constructor


void Keybindings::autoInit() { 
  //Initialize bindings automatically.
  //Function first tries to read keys.conf for bindings and if all bindings
  //still could not be set, hardcoded defaults will be used instead.

  std::ifstream file("keys.conf");
  std::string contents; //should really be a string stream
  
  if (file.is_open()) {
    //keys.conf was found
    //read and parse keys.conf
    file.close(); //close so the function can open it again

    std::cout << "keys.conf found." << std::endl;
    std::cout << "Attempting to parse keys.conf..." << std::endl;

    contents = autoInit_read(); //read keys.conf

    //parse keys.conf
    if (autoInit_parse(contents)) {
      std::cout << "Keys successfully loaded from keys.conf." << std::endl;
    } else {
      std::cout << "Failed to parse keys.conf; using default keys instead." << std::endl;
      defaultInit(); //if information is missing, use default keys instead
    } //end if statement
    
  } else {
    //keys.conf was not found
    //use hardcoded default keybinds
    std::cout << "Unable to load keys.conf; using default keys instead." << std::endl;
    defaultInit();
    
  } //end if statement

} //end autoInit function


std::string Keybindings::autoInit_read() {
   //read contents of keys.config
   //assumed file already exists and can be opened

   std::ifstream file("keys.conf");
   std::string curline;
   std::string contents; //should really be a string stream

   //read entire file and save to 'contents' for parsing
   while (!file.eof()) {
     file >> curline;
     contents += curline;
   } //end if statement

   file.close();
   return contents;

} //end autoInit_read functions


bool Keybindings::autoInit_parse(std::string contents) {
   //Parse contents of keys.config.
   //(in other words, bind the keys using info from the passed stream)
   //Return false if information is missing (not all keys could be bound)
   //Format of the stream is <Function>:<Keylist number>;

   unsigned int x_index = 0;
   unsigned int y_index = 0;

   std::string cur_function = "";
   std::string cur_keylist_number_s = "";
   int cur_keylist_number;

   //loop to go through each char of the passed string
   while (x_index < contents.size()) {

     //check for a delimiter (either a : or ;)
     if (contents[x_index+y_index] == ':') {
       //found a colon; extract a potential function from contents
       cur_function = contents.substr(x_index,y_index);

       x_index += ++y_index;
       y_index = 0;

     } else if (contents[x_index+y_index] == ';') {
       //found a semi-colon; extract a potential binding from contents
       cur_keylist_number_s = contents.substr(x_index,y_index);
       cur_keylist_number = std::stoi(cur_keylist_number_s); //convert to int

       //validate the extracted keybinding pair
       //validate the keylist number (should be between 0-80
       if ((cur_keylist_number > 0)&&(cur_keylist_number < 80)) {

	 //check if cur_function contains a recognized function
	 if (!std::strcmp(cur_function.c_str(),"Up")) {
	   bindings[0] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Down")) {
	   bindings[1] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Left")) {
	   bindings[2] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Right")) {
	   bindings[3] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Jump")) {
	   bindings[4] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Attack")) {
	   bindings[5] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Action")) {
	   bindings[6] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Menu")) {
	   bindings[7] = cur_keylist_number;

	 } else if (!std::strcmp(cur_function.c_str(),"Enter")) {
	   bindings[8] = cur_keylist_number;

	 } //end if statement

       } //end if statement

       x_index += ++y_index;
       y_index = 0;
     } //end if statement

     y_index++;
   } //end while loop

   return validateBindings();

} //end autoInit_parse function


void Keybindings::defaultInit() {
  //set bindings to hardcoded defaults

  bindings[0] = 73; //(Up) Look Up
  bindings[1] = 74; //(Down) Duck / Look Down
  bindings[2] = 71; //(Left) Move Left
  bindings[3] = 72; //(Right) Move Right
  bindings[4] = 41; //((Right) Control) Jump
  bindings[5] = 57; //(Space) Attack
  bindings[6] = 43; //((Right) Alt) Action
  bindings[7] = 36; //(Escape) Menu / Pause
  bindings[8] = 58; //(Enter) Enter / Confirm

} //end defaultInit function


bool Keybindings::validateBindings() {
  //validates the current keybindings
  //function checks if any keys are unset and if any bindings are duplicates
  
  bool valid = true;
  
  //check if all keys are bound
  for (int x = 0; x < 9; x++)
    if (bindings[x] == 101)
      valid = false;
  
  //check that all bindings are unique
  for (int x = 0; x < 9; x++)
    for (int y = x; y < 9; y++)
      if ((bindings[x]==bindings[y])&&(x!=y))
	valid = false;
   
  return valid;
} //end validateKeybindings function


void Keybindings::displayBindInfo() {
  //print out the keys currently bound in the bindings array
  
  std::cout << "Function: <Look up>\t";
  keylist[bindings[0]].displayKeyInfo(true);
  std::cout << "Function: <Look down/duck>\t";
  keylist[bindings[1]].displayKeyInfo(true);
  std::cout << "Function: <Move left>\t";
  keylist[bindings[2]].displayKeyInfo(true);
  std::cout << "Function: <Move right>\t";
  keylist[bindings[3]].displayKeyInfo(true);
  std::cout << "Function: <Jump>\t";
  keylist[bindings[4]].displayKeyInfo(true);
  std::cout << "Function: <Attack>\t";
  keylist[bindings[5]].displayKeyInfo(true);
  std::cout << "Function: <Action>\t";
  keylist[bindings[6]].displayKeyInfo(true);
  std::cout << "Function: <Pause/menu>\t";
  keylist[bindings[7]].displayKeyInfo(true);
  std::cout << "Function: <Enter>\t";
  keylist[bindings[8]].displayKeyInfo(true);

} //end displayBindInfo function


bool Keybindings::isKeyPressed(int x) {
  //named after SFML's isKeyPressed function but accepts an int instead.
  //function is also called without sf::Keyboards:: in front.
  //Jesus Christ this function...

  bool pressed = false;

  switch (x) {
    //case -1:
    //break;
  case 0: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::A); break;
  case 1: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::B); break;
  case 2: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::C); break;
  case 3: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::D); break;
  case 4: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::E); break;
  case 5: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F); break;
  case 6: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::G); break;
  case 7: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::H); break;
  case 8: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::I); break;
  case 9: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::J); break;
  case 10: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::K); break;
  case 11: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::L); break;
  case 12: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::M); break;
  case 13: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::N); break;
  case 14: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::O); break;
  case 15: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::P); break;
  case 16: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Q); break;
  case 17: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::R); break;
  case 18: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::S); break;
  case 19: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::T); break;
  case 20: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::U); break;
  case 21: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::V); break;
  case 22: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::W); break;
  case 23: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::X); break;
  case 24: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Y); break;
  case 25: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Z); break;
  case 26: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num0); break;
  case 27: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num1); break;
  case 28: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num2); break;
  case 29: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num3); break;
  case 30: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num4); break;
  case 31: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num5); break;
  case 32: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num6); break;
  case 33: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num7); break;
  case 34: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num8); break;
  case 35: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Num9); break;
  case 36: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Escape); break;
  case 37: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::LControl); break;
  case 38: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::LShift); break;
  case 39: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt); break;
  case 40: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::LSystem); break;
  case 41: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::RControl); break;
  case 42: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::RShift); break;
  case 43: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt); break;
  case 44: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::RSystem); break;
  case 45: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Menu); break;
  case 46: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::LBracket); break;
  case 47: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::RBracket); break;
  case 48: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::SemiColon); break;
  case 49: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Comma); break;
  case 50: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Period); break;
  case 51: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Quote); break;
  case 52: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Slash); break;
  case 53: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::BackSlash); break;
  case 54: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Tilde); break;
  case 55: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Equal); break;
  case 56: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Dash); break;
  case 57: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Space); break;
  case 58: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Return); break;
  case 59: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace); break;
  case 60: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Tab); break;
  case 61: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::PageUp); break;
  case 62: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::PageDown); break;
  case 63: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::End); break;
  case 64: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Home); break;
  case 65: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Insert); break;
  case 66: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Delete); break;
  case 67: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Add); break;
  case 68: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract); break;
  case 69: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Multiply); break;
  case 70: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Divide); break;
  case 71: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Left); break;
  case 72: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Right); break;
  case 73: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Up); break;
  case 74: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Down); break;
  case 75: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad0); break;
  case 76: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1); break;
  case 77: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2); break;
  case 78: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3); break;
  case 79: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad4); break;
  case 80: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad5); break;
  case 81: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad6); break;
  case 82: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7); break;
  case 83: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8); break;
  case 84: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad9); break;
  case 85: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F1); break;
  case 86: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F2); break;
  case 87: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F3); break;
  case 88: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F4); break;
  case 89: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F5); break;
  case 90: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F6); break;
  case 91: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F7); break;
  case 92: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F8); break;
  case 93: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F9); break;
  case 94: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F10); break;
  case 95: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F11); break;
  case 96: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F12); break;
  case 97: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F13); break;
  case 98: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F14); break;
  case 99: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F15); break;
  case 100: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Pause); break;
  case 101: pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::KeyCount); break;
  default: 
    pressed = false;
    break;
  } //end switch case

  return pressed;

} //end isKeyPressed function


bool Keybindings::isKeybindPressed(int in) {
  //checks if keybinding (up/down/jump/attack etc) was pressed
  //int being passed corresponds to the index for the bind array
  //for reference:
  //0=up,1=down,2=left,3=right,4=jump,5=attack,6=action,7=menu,8=enter

  bool pressed = false;
  if (isKeyPressed(bindings[in]))
    pressed = true;
  
  return pressed;

} //end isKeybindPressed function





void InputManager::init(EventDistributor* ined, Keybindings* inkb) {

  ed = ined;
  kb = inkb;

  add_listen("Event_Key_Pressed_Up");
  add_listen("Event_Key_Pressed_Down");
  add_listen("Event_Key_Pressed_Left");
  add_listen("Event_Key_Pressed_Right");
  add_listen("Event_Key_Pressed_Jump");
  add_listen("Event_Key_Pressed_Attack");
  add_listen("Event_Key_Pressed_Action");
  add_listen("Event_Key_Pressed_Menu");
  add_listen("Event_Key_Pressed_Enter");
  
} //end init function


sf::Event* InputManager::getEvent() { return &event; }


void InputManager::handle_event(char const* inevent) {
  //Handle an invididual event passed by the resolve_queue function in Event Manager
  //This event manager handles key and mouse inputs
  
  if (!std::strcmp(inevent,"Event_Key_Pressed_Up")) {
    std::cout << "Up" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Down")) {
    std::cout << "Down" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Left")) {
    std::cout << "Left" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Right")) {
    std::cout << "Right" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Jump")) {
    std::cout << "Jump" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Attack")) {
    std::cout << "Attack" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Action")) {
    std::cout << "Action" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Menu")) {
    std::cout << "Menu" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Key_Pressed_Enter")) {
    std::cout << "Enter" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Mouse_Down_Left")) {
    std::cout << "Click" << std::endl;
  } else if (!std::strcmp(inevent,"Event_Mouse_Moved")) {
    
  } else {
    //event could not be handled
    //maybe log it or something if you care
  } //end if statement
  
} //end handle_event function


void InputManager::getInput() {
  //Send events if keys pressed or mouse clicked

  if (event.type == sf::Event::KeyPressed) {
    if (kb->isKeybindPressed(0)) {
      fire_event("Event_Key_Pressed_Up");
    } else if (kb->isKeybindPressed(1)) {
      fire_event("Event_Key_Pressed_Down");
    } else if (kb->isKeybindPressed(2)) {
      fire_event("Event_Key_Pressed_Left");
    } else if (kb->isKeybindPressed(3)) {
      fire_event("Event_Key_Pressed_Right");
    } else if (kb->isKeybindPressed(4)) {
      fire_event("Event_Key_Pressed_Jump");
    } else if (kb->isKeybindPressed(5)) {
      fire_event("Event_Key_Pressed_Attack");
    } else if (kb->isKeybindPressed(6)) {
      fire_event("Event_Key_Pressed_Action");
    } else if (kb->isKeybindPressed(7)) {
      fire_event("Event_Key_Pressed_Menu");
      fire_event("Event_Gam_Exit_Request"); //TEMP
    } else if (kb->isKeybindPressed(8)) {
      fire_event("Event_Key_Pressed_Enter");
    } //end if statement
    
  } else if (event.type == sf::Event::MouseMoved) {

  } else if (event.type == sf::Event::MouseButtonPressed) {

  } else if (event.type == sf::Event::Resized) {
    
  } else if (event.type == sf::Event::LostFocus) {

  } else if (event.type == sf::Event::Closed) {
    //in the future maybe make this into a prompt
    fire_event("Event_Gam_Exit_Request");
    
  } //end if statement
} //end getInput function


