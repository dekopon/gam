//display.h

#include <iostream>
#include <cstring>
#include <vector>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#ifndef EVENT_H
#define EVENT_H
#include "event.h"
#endif

class Button {
  std::string name;

  int xpos, ypos;
  int length, height;

 public:
  Button(int,int,int,int,std::string);
  bool clicked(int,int);

  bool draw();

}; //end Button class


class Menu {

  std::string name;
  int id;

  //Button* buttons;
  std::vector<Button> buttons;
  int numButtons;

 public:
  Menu();
  bool checkClicked();

  bool draw();
  
  //~Menu();

}; //end Menu class


class WindowManager : public EventManager {

  sf::RenderWindow window;
  bool exit;

  sf::Clock clock;
  int framecount;
  
 public:
  void init(EventDistributor*);
  void handle_event(char const*);
  bool exit_status();

  void createWindow(int,int);
  sf::RenderWindow* getWindow();

  void fps_newframe();
  void fps_calculate();
  int get_fps();

}; //end WindowManager class


class DisplayManager : public EventManager {

  std::queue<char*> draw_list;
  WindowManager* wm;
  
public:
  void init(EventDistributor*, WindowManager*);
  void handle_event(char const*);
  void draw(sf::RenderWindow*);
  
}; //end DisplayManager class

