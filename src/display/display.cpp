//display.cpp
//Handles the displaying and graphics 


#include "display.h"



Button::Button(int inlength, int inheight, int inxpos, int inypos, std::string inname) {

  name = inname; //name of the button (maybe make this what the button says)sx
  
  length = inlength; //length of button
  height = inheight; //height of button
  xpos = inxpos; //x-coord of top-left corner of button
  ypos = inypos; //y-coord of top-left corner of button

} //end Button constructor


bool Button::clicked(int inxpos, int inypos) {
  //accepts the position of the mouse and returns true if
  //the mouse was in the area of the button when clicked
  //(mouse was assumed clicked if this function is called)

  bool clicked = false;

  if ((inxpos > xpos)&&
      (inypos > ypos)&&
      (inxpos < (xpos + length))&&
      (inypos < (ypos + height)))
    clicked = true;

  return clicked;

} //end clicked function



bool Button::draw() {
  //display the button
  //function returns false if the button could not be drawn

  bool status = true;

  sf::RectangleShape rectangle(sf::Vector2f(length,height));

  return status;

} //end draw function


Menu::Menu() {
  //set menu to empty state

  name = ""; //name of the menu (might be unnecessary)
  id = -1; //id of the menu

  numButtons = 0;

} //end Menu constructor


bool Menu::checkClicked() {
  //mouse was clicked
  //function queries every button to see if it was clicked

  bool buttonClicked = false;

  sf::Vector2i localPosition = sf::Mouse::getPosition();
  int xpos = localPosition.x;
  int ypos = localPosition.y;

  
  for (unsigned int x = 0; ((x < buttons.size())||buttonClicked == false); x++) {
    if (buttons[x].clicked(xpos,ypos)) {
      buttonClicked = true;
    } //end if statement
  } //end for loop

  return buttonClicked;

} //end checkClicked function


bool Menu::draw() {
  //draw the menu
  //function returns false if an error occurred

  bool status = true;

  //draw the menu background (or maybe other functions)
  //this doesn't exist yet


  //draw the buttons
  for (unsigned int x = 0; x < buttons.size(); x++)
    status = buttons[x].draw();

  
  return status;

} //end draw function





void WindowManager::init(EventDistributor* ined) {
  ed = ined;
  exit = false;
  framecount = 0;

  add_listen("Event_Gam_Exit_Request");

} //end init function


void WindowManager::handle_event(char const* inevent) {

  if (!std::strcmp(inevent,"Event_Gam_Exit_Request")) {
    exit = true;
  } //end if statement
  
} //end handle_event function


bool WindowManager::exit_status() {
  //WILL EXIT THE GAME IF bool exit = true!
  return exit;
} //end exit function


void WindowManager::createWindow(int x, int y) {
  //Create the window by accepting the dimensions as arguments.
  window.create(sf::VideoMode(x,y),"Gam (--)");

  //window.setVerticalSyncEnabled(true);
  window.setFramerateLimit(60);
  
} //end createWindow function


sf::RenderWindow* WindowManager::getWindow() { return &window; }


void WindowManager::fps_newframe() {
  //Called only by the display manager. Function is part of a set of functions to
  //keep count the fps. Function increases frame count by one when called.
  framecount++;
} //end frameToggle function


void WindowManager::fps_calculate() {
  //If one second has gone by since the last restart, function reports the fps
  //in the title of the window.

  //check if one second has ellapsed; do nothing if false
  if (clock.getElapsedTime().asMilliseconds() >= 1000) {

    std::string title;
    
    title = "Gam (" + std::to_string(framecount) + ")";
    window.setTitle(title);

    framecount = 0;
    clock.restart();
  } //end if statement

} //end fps_calculate function


int WindowManager::get_fps() {
  //return the fps for the current second
  return framecount;
} //end get_fps function



void DisplayManager::init(EventDistributor* ined, WindowManager* inwm) {
  ed = ined;
  wm = inwm;
} //end init function


void DisplayManager::handle_event(char const* inevent) {
  //Handle incoming events

  if (!std::strcmp(inevent,"Event_Menu_Start_Main")) {
    
  } else if (!std::strcmp(inevent,"Event_Menu_Start_Options")) {
    
  } else if (!std::strcmp(inevent,"Event_Menu_Start_PlayerSelect")) {
    
  } else if (!std::strcmp(inevent,"Event_Menu_Start_MapSelect")) {
    
  } else {
    //could not handle the event
  } //end if statement
  
} //end handle_event function


void DisplayManager::draw(sf::RenderWindow* window) {
  //Based on the state of the object, draw the screen.
  //In the future a clock/timer may need to be added to the object.

  window->clear(sf::Color::Black);

  

  window->display();
  wm->fps_newframe(); //increment frame count to window
  wm->fps_calculate();
  
} //end draw function

