#Makefile for gam
#There has to be a better way to do everything here...

#Compiler variables
CC:=g++
FLAGS:=-std=c++11 -Wall -g

#Directory variables
OBJ=obj
BIN=bin
SRC=src
INPUT=$(SRC)/input
DISPLAY=$(SRC)/display
EVENT=$(SRC)/event

#Library variables
SFMLLIB=-LC:/SFML/lib -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system

#Custom variables for varying OS's
ifeq ($(shell uname), Linux)
 #*nix (not Windows)
 SHELL=/bin/bash
 EXEC=gam
 MKDIRMET=mkdir -p
 SUPPRESSMET=> /dev/null 2>&1 || true
else
 #Windows
 SHELL=cmd.exe
 EXEC=gam.exe
 MKDIRMET=md
 SUPPRESSMET=2> nul || exit 0
endif


#Targets
.PHONY: default
.PHONY: all
.PHONY: gam.exe
default: gam
all: gam
gam.exe: gam

.PHONY: gam
gam: $(BIN)/$(EXEC)

$(BIN)/$(EXEC): $(OBJ)/gam.o $(OBJ)/input.o $(OBJ)/display.o $(OBJ)/event.o
	@-$(MKDIRMET) $(BIN) $(SUPPRESSMET)
	$(CC) $(FLAGS) $^ -o $@ $(SFMLLINK) $(SFMLLIB)

$(OBJ)/gam.o: $(SRC)/gam.cpp $(INPUT)/input.h $(DISPLAY)/display.h $(EVENT)/event.h
	@-$(MKDIRMET) $(OBJ) $(SUPPRESSMET)
	$(CC) $(FLAGS) -c $< -o $@ -I$(INPUT) -I$(DISPLAY) -I$(EVENT)

$(OBJ)/input.o: $(INPUT)/input.cpp $(EVENT)/event.h
	@-$(MKDIRMET) $(OBJ) $(SUPPRESSMET)
	$(CC) $(FLAGS) -c $< -o $@ -I$(EVENT)

$(OBJ)/display.o: $(DISPLAY)/display.cpp $(EVENT)/event.h
	@-$(MKDIRMET) $(OBJ) $(SUPPRESSMET)
	$(CC) $(FLAGS) -c $< -o $@ -I$(EVENT)

$(OBJ)/event.o: $(EVENT)/event.cpp
	@-$(MKDIRMET) $(OBJ) $(SUPPRESSMET)
	$(CC) $(FLAGS) -c $< -o $@


.PHONY: clean
clean:
	-rm $(OBJ)/*.o

.PHONY: uninstall
uninstall:
	-rm $(OBJ)/*.o
	-rm $(BIN)/*
	-rmdir $(OBJ)
	-rmdir $(BIN)
